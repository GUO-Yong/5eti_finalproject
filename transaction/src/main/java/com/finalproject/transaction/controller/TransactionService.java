package com.finalproject.transaction.controller;

import com.finalproject.transaction.model.CarteDTO;
import com.finalproject.transaction.model.Transaction;
import com.finalproject.transaction.model.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TransactionService {
    @Autowired
    TransactionRepository transactionRepository;


    //Ajouter la transaction
    public void addTransaction(Transaction transaction) {
        //ajouter la transaction
        transactionRepository.save(transaction);

        System.out.println("add transaction");
        System.out.println(transaction);
    }

    //retourner toutes les transactions
    public List<Transaction> getAllTransactions() {
        List<Transaction> transactions = new ArrayList<>();
        transactionRepository.findAll().forEach(transactions::add);
        return transactions;
    }

    //retourner la transaction dont id = ..
    public Transaction getTransactionById(int id) {
        Optional<Transaction> tOpt = transactionRepository.findById(id);
        if (tOpt.isPresent()) {
            return tOpt.get();
        }else {
            return null;
        }
    }

    //terminer la transaction(vendue)
    public void closeTransaction(Transaction transaction) {
        Integer idAcheteur = transaction.getIdAcheteur();

        addTransaction(transaction);

        int idVendeur = transaction.getIdVendeur();
        int idCarte = transaction.getIdCarte();

        int price = getCartePrice(idCarte);
        int portefeuille = getUserPortefeuille(idAcheteur);

        if( portefeuille>=price ) {
            //set portefeuille
            putUserPortefeuille(idVendeur, price);
            putUserPortefeuille(idAcheteur, -1*price);

            //remove carte
            postUserCarte(idAcheteur,idCarte);
            //changer owner
            putOwnerCarte( idAcheteur, idCarte);

            //update transaction state
            transaction.setClosed();
            addTransaction(transaction);

            System.out.println("close transaction: OK");

        }



    }


    private int getUserPortefeuille(int id) {
        ResponseEntity<UserDTO> resp = new RestTemplate().getForEntity("http://localhost:8081/users/"+id, UserDTO.class);
        return resp.getBody().getPortefeuille();
    }
    private int getCartePrice(int id) {
        ResponseEntity<CarteDTO> resp = new RestTemplate().getForEntity("http://localhost:8082/cartes/"+id, CarteDTO.class);
        return resp.getBody().getPrix();
    }


    private void putUserPortefeuille(int id, int price) {
        new RestTemplate().exchange("http://localhost:8081/users/"+id+"/portefeuille?price="+price, HttpMethod.PUT, null, Void.class);
    }


    private void postUserCarte(int idUser, int idCarte) {
        new RestTemplate().exchange("http://localhost:8081/users/"+idUser+"/cartes?idCarte="+idCarte, HttpMethod.POST, null, Void.class);
    }
    private void putOwnerCarte(int idOwner, int idCarte) {
        new RestTemplate().exchange("http://localhost:8082/cartes?idCarte="+idCarte+"&idOwner="+idOwner, HttpMethod.PUT, null, Void.class);
    }



    
    
}

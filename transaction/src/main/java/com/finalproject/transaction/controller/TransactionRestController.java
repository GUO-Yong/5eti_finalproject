package com.finalproject.transaction.controller;


import java.util.List;

import com.finalproject.transaction.model.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
public class TransactionRestController {

	@Autowired
	private TransactionService tService;
	
	//retourner toutes les transactions
	@RequestMapping(method=RequestMethod.GET,value="/transactions")
	public List<Transaction> getAllTransactions() {
		System.out.println("return all transactions");
		return tService.getAllTransactions();
	}
	
	//retourner la transaction dont id = ..
	@RequestMapping(method=RequestMethod.GET,value="/transactions/{id1}")
	public Transaction getMsg(@PathVariable String id1) {
		int id = Integer.parseInt(id1);
		Transaction transaction = tService.getTransactionById(id);
		return transaction;
	}
	
	// une nouvelle transaction
	@RequestMapping(method=RequestMethod.POST,value="/transactions")
	public void addTransaction(@RequestBody Transaction transaction) {
		tService.closeTransaction(transaction);


	}

}

package com.finalproject.transaction.controller;

import java.util.List;

import com.finalproject.transaction.model.Transaction;
import org.springframework.data.repository.CrudRepository;


public interface TransactionRepository extends CrudRepository<Transaction, Integer>{


	public List<Transaction> findByIdVendeur(int idVendeur);
	public List<Transaction> findByIdAcheteur(int idVendeur);
	public List<Transaction> findByIdCarte(int idCarte);

}


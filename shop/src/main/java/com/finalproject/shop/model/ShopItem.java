package com.finalproject.shop.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ShopItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    private Integer userId;
    private Integer cardId;
    public ShopItem() {
    }

    public ShopItem(Integer userId, Integer cardId) {
        this.userId = userId;
        this.cardId = cardId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public void setCardId(Integer cardId) {
        this.cardId = cardId;
    }

    public Integer getId() {
        return id;
    }

    public Integer getUserId() {
        return userId;
    }

    public Integer getCardId() {
        return cardId;
    }
}

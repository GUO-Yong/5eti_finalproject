package com.finalproject.shop.controller;

import com.finalproject.shop.model.ShopItem;
import org.springframework.data.repository.CrudRepository;
import java.util.List;
import java.util.Optional;

public interface ShopItemRepository extends CrudRepository<ShopItem, Integer> {
    Optional<ShopItem> findById(Integer id);
    List<ShopItem> findbyUserId(Integer id);
}

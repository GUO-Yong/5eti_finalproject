package com.finalproject.shop.controller;

import com.finalproject.shop.model.ShopItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
public class ShopRestController {
    @Autowired
    ShopService shopService;

    @RequestMapping(method= RequestMethod.GET,value="/shop/user/{idUser}")
    public List<ShopItem> getShopItemByUserId(@PathVariable Integer idUser){
        List<ShopItem> items = shopService.getShopItemByUserId(idUser);
        return items;
    }
    @RequestMapping(method = RequestMethod.GET,value="/shop/items")
    public List<ShopItem> getAllShopItem(){
        return shopService.getAllshopItem();
    }
    @RequestMapping(method = RequestMethod.POST,value = "/shop/additem")
    public void putItemOnShop(@RequestBody ShopItem shopItem){
        shopService.addShopItem(shopItem);

    }

}

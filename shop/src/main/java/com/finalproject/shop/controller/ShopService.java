package com.finalproject.shop.controller;

import com.finalproject.shop.model.ShopItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ShopService {
    @Autowired
    ShopItemRepository shopItemRepository;

    public void addShopItem(ShopItem shopItem){
        ShopItem createdItem = shopItemRepository.save(shopItem);
        System.out.println(createdItem);
        //supprimer la carte de l'utilisateur
        deleteUserCarte(shopItem.getUserId(),shopItem.getCardId());
    }

    public ShopItem getShopItemById(Integer id){
        Optional<ShopItem> hOpt = shopItemRepository.findById(id);
        if(hOpt.isPresent()){
            return  hOpt.get();
        }
        return null;
    }

    public List<ShopItem> getShopItemByUserId(Integer userId){
        List<ShopItem> items = shopItemRepository.findbyUserId(userId);
        return items;
    }

    public List<ShopItem> getAllshopItem(){
        List<ShopItem> items = new ArrayList<>();
        for(ShopItem it: shopItemRepository.findAll()){
            items.add(it);
        }
        return items;

    }


    private void deleteUserCarte(int idUser, int idCarte) {
        System.out.println("idUser"+idUser+"+"+idCarte+"-----------------------------------------------------------------------");
        System.out.println("http://localhost:8081/users/"+idUser+"/cartes?id="+idCarte);
        new RestTemplate().exchange("http://localhost:8081/users/"+idUser+"/cartes?idCarte="+idCarte, HttpMethod.DELETE, null, Void.class);
    }


}
